;(function (){

	// const marks
	const CELL_STATE = {
		NULL: null,
		player1: 'X',
		player2: 'O',
	};

	// state cell
	const cells = {
		1: {
			state: CELL_STATE.NULL, // could be X, O
			id: 1,
		},
		2: {
			state: CELL_STATE.NULL, // could be X, O
			id: 2,
		},
		3: {
			state: CELL_STATE.NULL, // could be X, O
			id: 3,
		},
		4: {
			state: CELL_STATE.NULL, // could be X, O
			id: 4,
		},
		5: {
			state: CELL_STATE.NULL, // could be X, O
			id: 5,
		},
		6: {
			state: CELL_STATE.NULL, // could be X, O
			id: 6,
		},
		7: {
			state: CELL_STATE.NULL, // could be X, O
			id: 7,
		},
		8: {
			state: CELL_STATE.NULL, // could be X, O
			id: 8,
		},
		9: {
			state: CELL_STATE.NULL, // could be X, O
			id: 9,
		},
	};

	// state game
	const gameState = {
		step: 0,
		playerWin: null,
		tempValArr: {
			first: null,
			second: null,
			third: null,
		}
	};

	// variants winner combination
	const winCombinations = [
		[1, 2, 3],
		[4, 5, 6],
		[7, 8, 9],
		[1, 4, 7],
		[2, 5, 8],
		[3, 6, 9],
		[1, 5, 9],
		[3, 5, 7],
	];

	// created game
	const $currentGame = document.querySelector('#game');
	$currentGame.appendChild(createGame());

	// listener by click on board game
	$currentGame.addEventListener('click', handlerGameFieldClick);

	// callback handlerGameFieldClick
	function handlerGameFieldClick(event) {
		let target = event.target;
		let player1 = gameState.step % 2 === 0;
		let player2 = gameState.step % 2 !== 0;

		if (target.getAttribute('class') === 'cell' ) {
			let dataId = target.getAttribute('data-id');

			if (player1 && !target.innerHTML) {
				if (!cells[dataId].state) {

					saveMark(dataId, CELL_STATE.player1);
					insertStep(target, CELL_STATE.player1);

					gameState.step++;
				}
			}

			if (player2 && !target.innerHTML) {
				if (!cells[dataId].state) {
					saveMark(dataId, CELL_STATE.player2);
					insertStep(target, CELL_STATE.player2);

					gameState.step++;
				}
			}

			const result = checkWinner(cells, winCombinations);

			if (result.winner) {
				gameState.playerWin = result.winner;
				insertClassWinner(gameState.tempValArr.first, gameState.tempValArr.second, gameState.tempValArr.third);
				gameOver(gameState.playerWin);
			}
		}
	}

	// insert ever step to board game
	function insertStep(player, mark) {
		player.innerHTML = mark;
	}

	// add class to element
	function addClass(elem, name) {
		elem.classList.add(name);
	}

	// save step in object state
	function saveMark(id, player) {
		cells[id].state = player;
	}

	// display who is winner
	function gameOver(winner) {
		if (winner) {
			alert('Winner is ' + winner + " !");
		}
	}

	// insert class winner to element and after highlights the winning combination in html by Css style
	function insertClassWinner(first, second, third) {
		const wrapper = document.querySelector('.wrapper');
		const arrCell = wrapper.querySelectorAll('.cell');

		arrCell.forEach((cell, index) => {
			if (index === cells[first].id - 1) addClass(cell, 'winner');
			if (index === cells[second].id - 1) addClass(cell, 'winner');
			if (index === cells[third].id - 1) addClass(cell, 'winner');
		});
	}

	// check winner combination
	function checkWinner(cells, winCombinations) {
		const result = {
			winner: null,
		};

		winCombinations.forEach(([first, second, third]) => {
			const stateFirst = cells[first].state;
			const stateSecond = cells[second].state;
			const stateThird = cells[third].state;

			if (stateFirst === CELL_STATE.NULL) {
				return null;
			}

			if (stateFirst === stateSecond && stateSecond === stateThird) {
				result.winner = stateFirst;
				gameState.playerWin = stateFirst;

				gameState.tempValArr.first = first;
				gameState.tempValArr.second = second;
				gameState.tempValArr.third = third;
			}
		});

		return result;
	}

	// template board game in html
	function createGame() {
		const table = document.createElement("div");
		table.className = "wrapper";

		for (let i = 0; i < 9; i++) {
			const div = document.createElement("div");
			div.className = "cell";

			div.setAttribute('data-id', i + 1);

			table.appendChild(div);
		}

		return table;
	}
}());
