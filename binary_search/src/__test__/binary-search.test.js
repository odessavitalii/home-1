import 'jest';
import binarySearch from './../binarySearch';

test('testing: binarySearch index 11 of arr equal 3', () => {
	// Arrange
	const arr = [1, 3, 9, 11, 12, 19, 33, 51, 56, 79];
	const num = 11;
	const expected = 3;

	// Act
	const actual = binarySearch(num, arr);

	// Assert
	expect(actual).toBe(expected);
});

test('testing: binarySearch index 11 of arr NOT equal 4', () => {
	// Arrange
	const arr = [1, 3, 9, 11, 12, 19, 33, 51, 56, 79];
	const num = 11;
	const expected = 4;

	// Act
	const actual = binarySearch(num, arr);

	// Assert
	expect(actual).not.toBe(expected);
});

test('testing: binarySearch index 11 of arr NOT equal 2', () => {
	// Arrange
	const arr = [1, 3, 9, 11, 12, 19, 33, 51, 56, 79];
	const num = 11;
	const expected = 2;

	// Act
	const actual = binarySearch(num, arr);

	// Assert
	expect(actual).not.toBe(expected);
});
