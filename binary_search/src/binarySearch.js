export default function binarySearch(target, data) {
	let start = 0;
	let middle = null;
	let end = data.length - 1;
	let result = -1;
	let found = false;

	while (!found && start <= end) {
		middle = Math.floor(start + (end - start) / 2);

		if (data[middle] === target) {
			found = true;
			result = middle;

		} else if (data[middle] > target) { // value lower part list
			end = middle - 1;

		} else {  // value upper part list
			start = middle + 1;

		}
	}

	return result;
}
